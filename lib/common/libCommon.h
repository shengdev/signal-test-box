/*
@file: libCommon.h
@author: ZZH
@date: 2023-02-03
@info: 动态库公共部分
*/
#pragma once

#include "calculatorConf.h"

using BasicType = CalculatorConf::BasicType;

extern "C" {
    //归一化数组
    void normalize(BasicType* output, BasicType* buf, unsigned int len);
    //交换数组前后两部分
    void swap_arr(BasicType* output, BasicType* buf, unsigned int len);
    //产生汉明窗
    void hamming(BasicType* buf, unsigned int len);
    //完整卷积, 会计算卷积核超出数据的部分
    void full_conv(BasicType* output, BasicType* arr1, size_t len1, BasicType* arr2, size_t len2);
    //有效卷积, 仅计算双方完全重合时的部分
    void valid_conv(BasicType* output, BasicType* arr1, size_t len1, BasicType* arr2, size_t len2);
}
