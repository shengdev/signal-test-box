#pragma once
#include "ui_mainwindow.h"

#include <QString>
#include <QDebug>
#include <QMainWindow>
#include <QtCore>
#include <QMessageBox>
#include <QPushButton>
#include <QtCharts/QChart>
#include <QtCharts/QChartView>
#include <QtCharts/QValueAxis>
#include <QtCharts/QLineSeries>

#include "LoggerWindow.h"
#include "calculatorConf.h"
#include "SignalSuffixManager.h"

class MainWindow: public QMainWindow
{
    Q_OBJECT
private:
    using BasicType = CalculatorConf::BasicType;
    
    SignalSuffixManager suffix;
    QString curItemText;
    QtCharts::QLineSeries* pSeries;
    QtCharts::QValueAxis* pAxisX, * pAxisY;
    QMenu* pMenu;
    LoggerWindow* logWindow;

    static const QString sigName;
    static const QRegExp sigNameRule;

    static const char* arrayKey;
    static const char* fsKey;
    static const char* fsUnitKey;
    static const char* calPointsKey;

    static const int calPointMax;
    static const int calPointMin;

protected:
    Ui::MainWindow ui;

public:
    static const int signalExpressRole = Qt::UserRole + 2;
    MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

    static const BasicType fsScale[3];

    void addSignal(void);
    void delSignal(void);
    void clrSignal(void);

    void enableExpress(void);

    void calculateCurSig(void);

    void itemChanged(QListWidgetItem* item);
    void currentItemChanged(QListWidgetItem* current, QListWidgetItem* previous);

    void importWorkspace(void);
    void exportWorkspace(void);

    void replaceNonStdChars(void);

public slots:
    void on_pCalNum_editingFinished(void);
};
