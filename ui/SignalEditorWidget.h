/*
@file: SignalEditor.h
@author: ZZH
@date: 2022-10-31
@info: 信号编辑器
*/
#pragma once
#include <QTextEdit>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include "CodeTip.h"
#include "log.h"

class SignalEditorWidget: public QWidget
{
    Q_OBJECT
private:
    QTimer* pCodeTipTimer;
    CodeTip* pCodeTip;
    SignalEditor* pTextEdit;
protected:

public:
    explicit SignalEditorWidget(QWidget* parent = nullptr): QWidget(parent)
    {
        this->pCodeTipTimer = nullptr;
        this->pCodeTip = nullptr;
        this->pTextEdit = nullptr;
    }

    ~SignalEditorWidget() {}

    void setUp(void);
    void requestCodeTip(void);

    virtual void keyPressEvent(QKeyEvent* e) override;
};
