/*
@file: CodeTip.h
@author: ZZH
@date: 2022-11-04
@info: 代码提示框
*/
#pragma once
#include <QObject>
#include <QListWidget>
#include <QTimer>
#include <QFocusEvent>
#include <QKeyEvent>
#include "SignalEditor.h"
#include "log.h"

class CodeTip:public QListWidget
{
    Q_OBJECT
private:
    SignalEditor* bindEditor;
protected:

public:
    explicit CodeTip(SignalEditor* bind, QWidget* parent = nullptr);
    ~CodeTip() {}

    void focusOutEvent(QFocusEvent* e) override;
    virtual void keyPressEvent(QKeyEvent* e) override;
    bool eventFilter(QObject *object, QEvent *event) override;

    void requestCodeTip(const QPoint& pos, const QString& code);//外部请求代码提示的接口

// signals:
//     void selectItemDone(QListWidgetItem* item) const;
};
