/*
@file: SignalEditor.cpp
@author: ZZH
@date: 2022-11-18
@info: 信号编辑器
*/
#include "SignalEditor.h"
#include "log.h"

void SignalEditor::keyPressEvent(QKeyEvent* e)
{
    this->setFocus(Qt::ActiveWindowFocusReason);
    QTextEdit::keyPressEvent(e);
}

void SignalEditor::addSelectedItem(QListWidgetItem* item)
{
    QString textToAdd = item->data(Qt::UserRole + 1).toString();
    int numOfArg = item->data(Qt::UserRole + 2).toInt();
    int offset = 0;

    this->setFocus(Qt::ActiveWindowFocusReason);

    if (numOfArg >= 0)
    {
        textToAdd.append('(');
        for (int i = 1;i < numOfArg;i++)
            textToAdd.append(", ");
        textToAdd.append(')');

        offset = numOfArg * 2 - 1;

        if (offset < 0)//如果函数没有参数, 例如rand, 这种情况下不移动光标, 因为不需要输入参数
            offset = 0;
    }

    this->blockSignals(true);
    this->insertPlainText(textToAdd);
    this->blockSignals(false);

    auto tc = this->textCursor();
    tc.setPosition(tc.position() - offset);
    this->setTextCursor(tc);
}
